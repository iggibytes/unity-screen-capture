# Unity - Screen Capture #


### What is this repository for? ###

* Unity Editor Window that provides user interface for Application.CaptureScreenshot(...) method.
* [https://docs.unity3d.com/ScriptReference/Application.CaptureScreenshot.html](https://docs.unity3d.com/ScriptReference/Application.CaptureScreenshot.html)

### How do I get set up? ###

* Download and import "unity-screen-capture.unitypackage"


### Screenshot ###

![Screenshot 2017-03-19 13.31.11.png](https://bitbucket.org/repo/rpKMaRM/images/932709159-Screenshot%202017-03-19%2013.31.11.png)


### Licence ###

* CC0 1.0 Universal (CC0 1.0)
* https://creativecommons.org/publicdomain/zero/1.0/